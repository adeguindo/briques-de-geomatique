..  _terrain:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Capture de données sur le terrain
===================================

Nous allons voir dans cette partie comment capturer des données sur le terrain, comme des traces GPS ou des photos localisées dans l'espace.

.. contents:: Table des matières
    :local:

Capture d'une trace GPS
-------------------------

Capturer une trace GPS peut être utile lorsque nous sommes amenés à faire du terrain. Nous allons voir ici comme capturer facilement une telle trace avec un smartphone.

Concrètement, nous allons régler notre smartphone de telle sorte qu'il enregistre notre localisation GPS à des intervalles de temps réguliers pendant une durée définie par l'utilisateur. Des arcs relieront automatiquement ces points individuels afin de les assembler en *trace*. Cette trace sera sauvegardée en format *.gpx*, format standard pour ce type de fichier. Ce fichier *gpx* pourra ensuite être importé dans un logiciel SIG comme QGIS pour l'intégrer à un projet géomatique.

Trace GPS avec l'application OsmAnd
*************************************
L'application `OsmAnd`_ est basée sur le projet de carte collaborative ̀ OpenStreetMap`_. OpenStreetMap est à la cartographie ce que Wikipedia est à l'encyclopédie. C'est une carte globale construite par une communauté de contributeurs. C'est une donnée reconnue pour sa qualité même si cette dernière est forcément inégale d'un endroit du monde à un autre. Cependant, d'une manière générale, nous pouvons totalement nous y fier.

OsmAnd est l'application pour smartphone qui permet d'utiliser ces cartes de façon mobile. OsmAnd est disponible pour Android et IOS gratuitement. Il existe une version payante qui offre des possibilités supplémentaires, mais la version gratuite est largement suffisante pour ce que nous souhaitons faire.

.. _OsmAnd: http://osmand.net/
.. _OpenStreetMap: https://www.openstreetmap.org

.. tip::
	En passant par le dépôt `F-Droid`_, il est possible de télécharger gratuitement la version intégrale de OsmAnd.
	
.. _F-Droid: https://f-droid.org/
	
Installation et configuration
++++++++++++++++++++++++++++++++++
Une fois l'application installée, au premier lancement il est proposé de télécharger la carte de la région dans laquelle nous nous trouvons. Ce téléchargement du fond de carte nous permettra d'utiliser cette application en mode hors-ligne. L'écran principal affiche notre localisation et la direction dans laquelle nous pointons notre appareil sur le fond de carte OSM. En bas à gauche de l'écran nous pouvons accéder au menu général (:numref:`osm-base`).

.. figure:: figures/fig_osmand_base_menu.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-base
    
    Vue générale (à gauche) et accès au menu de OsmAnd (à droite).

Nous allons maintenant charger le plugin (aussi appelé *greffon*) qui permet d'enregistrer une trace GPS. Pour cela nous allons dans le menu et nous sélectionnons l'entrée *Greffons supplémentaires* (ou *Plugins* selon la langue d'installation). Nous allons activer le deux greffon permettant d'enregistrer une trace *Trip recording* et celui permettant de prendre une note (audio, vidéo ou photo) géoréférencée *Audio/video notes* (:numref:`osm-greffon`).

.. figure:: figures/fig_osmand_greffon.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-greffon
    
    Activation de greffons supplémentaires.

Pour activer ces greffons, il suffit de cliquer sur l'icône |icone_osmand_explore| et de sélectionner *Activer*. Une fois cette activation effectuée, si ces greffons n'apparaissent pas automatiquement sur la vue générale, nous pouvons activer leur affichage. Cela se fait via le menu général en choisissant l'entrée *Configure screen*. Le menu de configuration de l'écran s'affiche et nous pouvons alors choisir les greffons à afficher sur la vue générale. Nous sélectionnons *Trip recording* et *Audio/video notes*. Il est également intéressant d'afficher l'altitude et les informations relatives au signal GPS (:numref:`osm-aff-greffon`).

.. figure:: figures/fig_osmand_affichage_greffon.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-aff-greffon
    
    Configuration de l'écran (à gauche) et affichage des greffons (à droite).

Capturer et sauvegarder une trace
++++++++++++++++++++++++++++++++++++

L'option d'enregistrement de trace et les autres greffons apparaissent en haut à droite de l'écran. Pour lancer un enregistrement, nous cliquons sur *REC*. Un menu nous demandant l'intervalle de temps entre deux enregistrements de points s'ouvre. Nous pouvons laisser la valeur par défaut (:numref:`osm-rec`).

.. warning::
	À priori plus cet intervalle de temps sera court, plus la trace sera précise. Néanmoins, la position enregistrée n'est pas parfaite, surtout avec un smartphone. Ainsi, dans un déplacement de position enregistrée il y a une part de "vrai" déplacement mais également une part de bruit.

.. figure:: figures/fig_osmand_rec.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-rec
    
    Mise en route de l'enregistrement (à gauche) et configuration du pas de temps de l'enregistrement.

Lorsque l'enregistrement est en cours, un point rouge apparaît au niveau du menu d'enregistrement. Pour mettre fin à l'enregistrement, il suffit de recliquer sur le menu *REC* (:numref:`osm-stop`).

.. figure:: figures/fig_osmand_stop.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-stop
    
    Sauvegarder la trace GPS (à gauche) et la nommer (à droite).

Pour sauvegarder la trace, nous sélectionnons *Save current track*. D'autres choix sont possibles comme la mise en pause de la capture, ce qui peut être intéressant lorsqu'on s'arrête à une étape. Il faut ensuite nommer sa trace.

Visualiser et exporter une trace
++++++++++++++++++++++++++++++++++
Une fois la trace capturée, elle s'affiche automatiquement sur la vue générale de l'application. Si ce n'est pas le cas, il suffit de se rendre dans le menu principal et de sélectionner *Configure map* (:numref:`osm-aff-trace`). 

.. figure:: figures/fig_osmand_affichage_trace.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-aff-trace
    
    Affichage de la trace GPS (à gauche) et choix de l'affichage (à droite).

Via le menu de configuration de la carte, il est possible d'afficher et de désafficher la trace enregistrée. En cliquant sur l'icône |icone_osmand_choix|, il est possible de faire de même avec d'autres traces qui auraient été capturées précédemment.

.. |icone_osmand_choix| image:: figures/icone_osmand_choix.png
              :width: 25 px

Il est possible d'accéder à plus de détails concernant notre trace en se rendant dans le menu principal et en sélectionnant le *Dashboard* (:numref:`osm-trace-details`).

.. figure:: figures/fig_osmand_trace_details.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-trace-details
    
    Sélection de la trace GPS (à gauche) et affichage des détails (à droite).

Dans le *Dashboard*, nous trouvons un panneau contenant les traces. Lorsque nous cliquons sur une trace, ses détails s'affichent. Nous retrouvons la trace sur la carte mais également d'autres éléments. Sur le graphique central, nous trouvons le linéaire de la trace assorti de l'évolution de l'altitude le long de ce linéaire (en bleu) et de l'évolution de la vitesse lors du capture de la trace (en orange). Si nous sélectionnons l'option *Show on map*, nous synchronisons la position sur le graphique et sur la carte. Au dessous, nous retrouvons d'autres informations comme la distance totale, la durée de l'enregistrement, et les dates de début et de fin.

Nous pouvons également exporter notre trace à partir de ce menu. En cliquant sur l'icône de partage |icone_osmand_partage|, nous pouvons par exemple nous l'envoyer par mail.

.. |icone_osmand_partage| image:: figures/icone_osmand_partage.png
              :width: 20 px

.. note::
	Au final, nous disposons là d'un outil très intéressant et plutôt complet lorsque nous souhaitons capturer une trace GPS sans autre matériel que son smartphone.

Une fois la trace GPS exportée, il est possible de l'importer dans un logiciel SIG (:ref:`importer-des-points-GPS`).


..  _photo-localisee:

Prendre une photo géoréférencée
------------------------------------

Lorsque nous arpentons notre terrain d'études ou lors d'une simple randonnée il peut être intéressant de prendre une photo avec sa localisation GPS. Cette localisation nous permettra ensuite d'inclure cette photo dans un projet de géomatique.

Cette manipulation peut, par exemple, être utile lorsque nous souhaitons confronter une image satellite à la réalité terrain. Les photos peuvent alors servir de validation à une classification d'image satellite.

Prendre une photo géolocalisée en "natif" avec son smartphone
****************************************************************
Il est possible d'associer automatiquement la géolocalisation aux photos prises avec son smartphone. Ainsi, dès qu'une photo est prise, ses métadonnées incluront les coordonnées géographiques de la prise de vue et même parfois la direction visée.

Pour activer cette fonctionnalité, il suffit d'aller dans les réglages de la caméra au moment de prise de la photo. Dans les réglages, il y a une ligne qui permet d'activer ou non la localisation des photos. Cette méthode a l'avantage d'éviter de passer par une application tierce pour prendre des photos géolocalisées.

.. warning::
	Avec cette méthode, toutes vos photos seront géolocalisées. Attention, si vous partagez vos photos sur les réseaux sociaux, tout le monde sera capable de vous tracer.

Prendre une photo géolocalisée avec l'application OsmAnd
**********************************************************
Nous montrerons ici la procédure à suivre pour prendre ce genre de photos avec l'application *OsmAnd*. Pour des détails sur cette application, référez vous à la section consacrée à la capture de traces GPS. Il est tout d'abord nécessaire d'activer le module *Audio/video notes* dans le gestionnaire de greffons accessible via le menu général (:numref:`osm-photo-start`).

.. figure:: figures/fig_osmand_photo_start.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-photo-start

    Activation du greffon de prises de notes audios et vidéos (à gauche) et ouverture du menu de prises de photos (à droite).

Une fois ce greffon installé, une icône *Start* apparaît en haut à droite de la vue générale. Pour prendre une photo, il suffit de cliquer dessus (:numref:`osm-photo-prise`).

.. figure:: figures/fig_osmand_photo_prise.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-photo-prise

    Prise de photo (à gauche) et localisation de la photo (à droite).

Une fenêtre s'ouvre nous demandant si nous souhaitons prendre une note audio (i.e. un enregistrement audio), une vidéo ou une photo. Nous sélectionnons le dernier choix. La fonction d'appareil photo classique du smartphone s'ouvre alors. Une fois la photo prise, une icône de photo apparaît à l'emplacement précis de la prise de vue sur la vue générale de OsmAnd.

.. note::
	Si nous choisissons de prendre un enregistrement audio ou vidéo et que nous nous déplaçons pendant la prise de son ou de vue, la localisation restera celle du déclenchement de l'enregistrement.

Il est ensuite possible de voir les détails de la photo prise. Pour cela, nous retournons dans le menu principal et nous ouvrons le *Dashboard* (:numref:`osm-photo-details`).

.. figure:: figures/fig_osmand_photo_details.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-photo-details

    Liste des photos (à gauche) et détails de la photo sélectionnée (à droite).

Dans ce *Dashboard*, nous trouvons un panneau *Audio/video notes* dans lequel sont listées toutes nos photos et autres notes. Si nous cliquons sur notre photo, sa position sur la carte apparaît. Nous pouvons également voir la photo sur cette même vue en tirant la fenêtre vers le haut.

Notons qu'il est tout à fait possible de renommer une photo si le nom par défaut ne nous convient pas. Il suffit d'aller dans le menu général et dans le menu *My Places* et dans l'onglet *A/V Notes* (:numref:`osm-photo-rename`).

.. figure:: figures/fig_osmand_photo_rename.png
    :width: 16em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-photo-rename

    Renommer, effacer ou exporter une photo.

En cliquant sur l'icône |icone_osmand_explore|, nous trouvons les outils de manipulation de l'image, notamment celui pour la renommer. Nous pouvons également exporter notre photo à partir de ce menu en cliquant sur l'icône de partage |icone_osmand_partage2|.

.. |icone_osmand_explore| image:: figures/icone_osmand_explore.png
              :width: 15 px

.. |icone_osmand_partage2| image:: figures/icone_osmand_partage2.png
              :width: 20 px

.. note::
	Il aurait été pratique de pouvoir associer une petite note écrite à une photo afin de donner quelques éléments de contexte.

Une fois des photos localisées dans l'espace capturées, il est possible de les importer dans un logiciel SIG comme QGIS (:ref:`importer-photos-localisees`).
